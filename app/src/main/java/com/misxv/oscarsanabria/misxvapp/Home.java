package com.misxv.oscarsanabria.misxvapp;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.Image;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.support.v7.app.NotificationCompat;
import android.app.NotificationManager;
import android.content.SharedPreferences;


public class Home extends AppCompatActivity implements View.OnClickListener {

    final private int PICK_IMAGE = 1;
    final private int CAPTURE_IMAGE = 2;
    private static final int CAMERA_REQUEST = 1888;
    private android.hardware.Camera mCamera = null;
    private CameraView mCameraView = null;
    ImageView imagenNueva;
    Intent i;
    Bitmap bmp;
    final static int cons = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_home);
        TextView tv = (TextView) this.findViewById(R.id.txtFecha);

        SharedPreferences preferencias = this.getSharedPreferences("datos",Context.MODE_PRIVATE);

        tv.setText("La gran fecha es:"+preferencias.getString("fechaGuardada",""));

        Button scan_button = (Button) findViewById(R.id.btnCamara);
        scan_button.setOnClickListener(this);

        imagenNueva = (ImageView) findViewById(R.id.FotoPerfil);



    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");

    }

    public  void NotificacionSimple(int id, int iconId, String titulo, String contenido) {

        //Constructor de la Notificacion
        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        //Aqui se selecciona el icono que se va a mostrar
                        .setSmallIcon(R.drawable.ic_stat_name)
                        //Aqui pones el titulo de la notificacion
                        .setContentTitle(titulo)
                        //Aqui pones el  contenido del titulo
                        .setContentText(contenido)
                        //Seleccionas un color para la notificacion
                        .setColor(getResources().getColor(R.color.colorPrimaryDark))
                        //Selecciones el nivel  de seguridad que va a tener la notificacione
                        .setVisibility(NotificationCompat.VISIBILITY_SECRET);


        // Construir la notificación y emitirla

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(id, builder.build());

    }

    @Override
    public void onClick(View v) {

        int id ;
        id = v.getId();

        switch (id) {
            case R.id.btnCamara:
                //i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //startActivityForResult(i, cons);
                final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAPTURE_IMAGE);
                break;


        }
    }
    @Override
    protected  void onActivityResult (int requestCode, int resultCode, Intent data)
    {
       super.onActivityResult(requestCode,resultCode,data);
        if(resultCode == Activity.RESULT_OK)
        {
            Bundle ext = data.getExtras();
            bmp  = (Bitmap)ext.get("data");
            imagenNueva.setImageBitmap(bmp);
        }

        /*if (resultCode != Activity.RESULT_CANCELED) {
            if (requestCode == PICK_IMAGE) {
                Bundle ext = data.getExtras();
                bmp  = (Bitmap)ext.get("data");
                img.setImageBitmap(bmp);
            } else if (requestCode == CAPTURE_IMAGE) {
                Bundle ext = data.getExtras();
                bmp  = (Bitmap)ext.get("data");
                img.setImageBitmap(bmp);
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }*/

        /*if (requestCode == CAMERA_REQUEST)
            if (data != null)
            {
                NotificacionSimple(1,  R.drawable.ic_stat_name,"Mis XV App","Tu foto ha sido cargada!");
                Bundle ext = data.getExtras();
                bmp  = (Bitmap)ext.get("data");
                imagenNueva.setImageBitmap(bmp);
            }*/

    }
}



