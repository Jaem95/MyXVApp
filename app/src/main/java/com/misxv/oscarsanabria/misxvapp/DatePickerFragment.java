package com.misxv.oscarsanabria.misxvapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by Jose Antonio Escobar on 25/10/2016.
 */


public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private NotificationManager notifyMgr;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);





    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        TextView tv = (TextView) getActivity().findViewById(R.id.txtFecha);
    /*    tv.setText("Date changed...");
        tv.setText(tv.getText() + "\nYear: " + year);
        tv.setText(tv.getText() + "\nMonth: " + month);
        tv.setText(tv.getText() + "\nDay of Month: " + dayOfMonth);*/

        month = month+1;

        String stringOfDate = dayOfMonth + "/" + month + "/" + year;
        tv.setText( "La nueva fecha es: " + dayOfMonth + "/" + month + "/" + year);


        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
        String strFecha = dayOfMonth + "/" + month + "/" + year;
        try {
            Date fecha = formatoDelTexto.parse(strFecha);
            guardarInformacion(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }










    }

    public  void guardarInformacion(Date fecha)
    {


        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
        SharedPreferences preferencias = this.getActivity().getSharedPreferences("datos",Context.MODE_PRIVATE);
        Editor editor  = preferencias.edit();
        editor.putString("fechaGuardada",sf.format(fecha));
        System.out.println("--------+++++++"+sf.format(fecha));
        editor.commit();
        try {
            finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }


    }



}
